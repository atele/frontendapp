/**
 * Created by stikks-workstation on 2/25/17.
 */
'use strict';
var app = angular.module('atele.routes', ['ngRoute']);

app.config(['$routeProvider', function($routeProvider){
    $routeProvider
        .when('/',{
            templateUrl: 'partials/client/overview'
        })
        .when('/networks',{
            templateUrl: 'partials/client/dashboard/network',
            controller: 'NetworkDashboardController',
            resolve: {
                data: function (Network, $rootScope) {
                    return Network.query({client_id: $rootScope.client.id}).$promise.then(function(data){
                      return data;
                    });
                }
            }
            //resolve: {
            //    data: [
            //        '$rootScope', 'Network', function ($rootScope, Network, $timeout) {
            //
            //        }
            //    ]
            //}
        })
        .when('/installations',{
            templateUrl: 'partials/client/dashboard/installation'
        })
        .when('/customers', {
            templateUrl: 'partials/client/dashboard/customer'
        })
        .when('/reports', {
            templateUrl: 'partials/client/dashboard/report'
        })
        .otherwise({redirectTo:'/'});
}]);
