// /**
//  * Created by stikks-workstation on 2/25/17.
//  */
var app = angular.module('atele.controllers.alert', ["pubnub.angular.service", 'atele.models', 'atele.services']);

// ========================
// alert controllers
// ========================

app.controller('AlertsController', ['$scope', 'FactotumService', 'installation', 'data', function ($scope, FactotumService, installation, data) {
    $scope.installation = installation;
    $scope.data = FactotumService.buildPage(data);
}]);

app.controller('AlertController', ['$scope', '$rootScope', 'alert', function ($scope, $rootScope, alert) {
    console.log(alert);
}]);


app.controller('AddAlertController', ['$scope', '$rootScope', '$state', 'Alert', 'alert_types', 'installation', 'Device', 'Installation',
    function ($scope, $rootScope, $state, Alert, alert_types, installation, Device, Installation) {

        $scope.alert_types = alert_types.results;
        $scope.installation = new Installation(installation);
        $scope.formData = {
            service_id: $rootScope.service.id,
            client_id: $rootScope.client.id,
            installation_id: $scope.installation.id,
            station_id: $scope.installation.station_id,
            level: 'warning'
        };

        $scope.save = function () {
            $scope.formData.is_warning = $scope.formData.level == 'warning';
            $scope.formData.is_critical = $scope.formData.level == 'critical';

            var alert = new Alert($scope.formData);
            alert.$save(function (data, status) {
                $state.go('installation_details', {id: $scope.installation.id})
            }, function (err) {
                $scope.data.error = err.data;
            });
        }
    }]);


// ========================
// end of alert controllers
// ========================
