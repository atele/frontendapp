/**
 * Created by stikks-workstation on 2/10/17.
 */
'use strict';

var app = angular.module('atele.controllers.equipment', ["pubnub.angular.service", 'atele.models', 'atele.services']);

// ========================
// network controllers
// ========================

app.controller('NetworkDashboardController', ['FactotumService', '$scope', 'data', function (FactotumService, $scope, data) {
    $scope.data = FactotumService.buildPage(data);
    $scope.stations = $scope.data.objects.map(function (elem) {
        return elem.station
    });
    $scope.sliced_objects = $scope.data.objects.slice(0,2);
}]);


app.controller('NetworkRequestsController', ['FactotumService', '$scope', 'objects', 'Station', function (FactotumService, $scope, objects, Station) {
    $scope.data = {objects: objects.data};
    $scope.data.objects = objects.data.map(function (elem) {
        var station = Station.get({id: elem.station_id});
        station.$promise.then(function (resp) {
            elem.station = resp
        });
        return elem;
    });
}]);

//app.controller('networkDeviceController', ['FactotumService', '$scope', 'network', 'network', function (FactotumService, $scope, network, network) {
//    $scope.network = new network(network);
//    $scope.devices = $scope.network.devices.map(function (elem) {
//        return elem.network_id != $scope.network.id
//    });
//}]);


app.controller('NetworkController', ['FactotumService', '$scope', 'data', function (FactotumService, $scope, data) {
    $scope.data = FactotumService.buildPage(data);
}]);

app.controller('NetworkDetailController', function ($scope, $rootScope, Network, network, DeviceService, Fault, $interval, requests, Device, $timeout) {
    $scope.network = new Network(network);

    $scope.devices = $scope.network.devices.filter(function (elem) {
        return elem.is_active;
    });

    $scope.node_devices = $scope.devices.filter(function (resp) {
        return resp.is_node && !resp.is_coordinator;
    });

    var active_devices = $scope.devices.filter(function (res) {
        return res.is_active;
    });
    $scope.sliced_devices = active_devices.slice(0, 2);

    $scope.active_count = active_devices.length;
    $scope.device_requests = requests.data;

    $scope.metrics = [];
    $scope.nodes = [];

    active_devices.map(function (itr) {
        Device.query({id: itr.id}).$promise.then(function (ras) {
            $scope.nodes.push(ras)
        });
    });

    $timeout(function () {
        $.each($scope.nodes, function (pos, elem) {
            elem.product.metrics.map(function (met) {

                if (met.devices == undefined) {
                    met.devices = []
                }

                if (met.devices.indexOf(elem) == -1) {
                    met.devices.push(elem)
                }

                if ($scope.metrics.indexOf(met.slug) == -1) {
                    $scope.metrics.push(met)
                }
            });
        });
    }, 500);

});

app.controller('AddNetworkController', ['$scope', '$rootScope', 'products', 'stations', 'Network', '$state', 'prevState', function ($scope, $rootScope, products, stations, Network, $state, prevState) {

    $scope.products = products.results;
    $scope.stations = stations.results;
    $scope.formData = {client_id: $rootScope.client.id};

    $scope.save = function () {
        var network = new Network($scope.formData);
        network.$save(function (data, status) {
            $state.go(prevState.Name, prevState.Params);
        }, function (err) {
            $rootScope.data.error = err.data
        });
    }
}]);

// ========================
// end of network controllers
// ========================

// ========================
// device controllers
// ========================

app.controller('DeviceController', ['FactotumService', '$scope', 'data', 'installation', function (FactotumService, $scope, data, installation) {
    $scope.data = FactotumService.buildPage(data);
    $scope.installation = installation;
}]);

app.controller('AddDeviceController', ['$scope', '$rootScope', '$state', 'prevState', 'installation', 'PopupService', 'DeviceRequest',
    function ($scope, $rootScope, $state, prevState, installation, PopupService, DeviceRequest) {

        $scope.products = $rootScope.service.products.filter(function (elem) {
            if (elem.is_network == false && elem.is_alert == false) {
                return elem
            }
        });

        $scope.installation = installation;
        $scope.deviceForm = {installation_id: $scope.installation.id};

        $scope.send = function () {
            var req = new DeviceRequest($scope.deviceForm);
            req.$save(function (data, status) {
                $state.go(prevState.Name, prevState.Params)
            }, function (err) {
                $rootScope.data.error = err.data
            });
        }
    }]);

app.controller('NetworkDeviceController', ['$scope', '$rootScope', '$state', 'prevState', 'PopupService', 'NetworkRequest', 'devices', 'Network', 'network',
    function ($scope, $rootScope, $state, prevState, PopupService, NetworkRequest, devices, Network, network) {

        $scope.network = new Network(network);
        $scope.devices = [];

        devices.results.map(function (elem) {
            if (elem.network_id !== $scope.network.id) {
                $scope.devices.push(elem);
            }
        });

        $scope.deviceForm = {client_id: $rootScope.client.id, network_id: $scope.network.id};

        $scope.send = function () {
            var req = new NetworkRequest($scope.deviceForm);
            req.$save(function (data, status) {
                $state.go(prevState.Name, prevState.Params);
            }, function (err) {
                $rootScope.data.error = err.data
            });
        }
    }]);

// ========================
// end of device controllers
// ========================

// ========================
// installation controllers
// ========================

app.controller('InstallationsController', ['$scope', 'FactotumService', 'data', 'Installation', 'ChartService', 'Device', '$timeout',
    function ($scope, FactotumService, data, Installation, ChartService, Device, $timeout) {
        $scope.data = FactotumService.buildPage(data);
        $scope.devices = [];

        $scope.data.objects.map(function (resp) {
            $scope.devices = $scope.devices.concat(resp.devices);
        });

        $scope.sliced_objects = $scope.data.objects.slice(0, 2);

        $scope.metrics = [];
        var active_devices = $scope.devices.filter(function (res) {
            return res.is_active;
        });
        $scope.active_devices = [];

        active_devices.map(function (itr) {
            Device.query({id: itr.id}).$promise.then(function (ras) {
                $scope.active_devices.push(ras)
            });
        });

        $timeout(function () {
            $.each($scope.active_devices, function (pos, elem) {
                elem.product.metrics.map(function (met) {

                    if (met.devices == undefined) {
                        met.devices = []
                    }

                    if (met.devices.indexOf(elem) == -1) {
                        met.devices.push(elem)
                    }

                    if ($scope.metrics.indexOf(met.slug) == -1) {
                        $scope.metrics.push(met)
                    }
                });
            });
        }, 500);
    }]);

app.controller('InstallationController', function ($scope, FactotumService, data, Installation) {
    $scope.data = FactotumService.buildPage(data);
    $scope.data.objects = $scope.data.objects.map(function (elem) {
        return new Installation(elem);
    });
});

app.controller('InstallationDetailController', function ($scope, $rootScope, installation, Installation, DeviceService, Fault, $interval,
                                                         ChartService, requests, Device, alerts, Alert) {
    $scope.installation = new Installation(installation);
    $scope.active_devices = $scope.installation.devices.filter(function (elem) {
        return elem.is_active;
    });

    $scope.alerts = alerts.results.map(function (elem) {
        return new Alert(elem);
    });

    $scope.active_count = $scope.active_devices.length;

    var queryParams = {installation_id: $scope.installation.id};
    Fault.query(queryParams).$promise.then(function (elem) {
        $scope.faults = elem.data
    });

    Device.query(queryParams).$promise.then(function (dev) {
        $scope.installation.devices = dev.results;
        $scope.installation.active_devices = $scope.installation.devices.filter(function (res) {
            return res.is_active;
        });
        $scope.active_devices = $scope.installation.active_devices.slice(0, 2);
    });

    $scope.device_requests = requests.data;
    $scope.deviceService = DeviceService;

    $scope.buildGauge = ChartService.buildGauge;
    $scope.buildChart = ChartService.buildChart;

});

app.controller('InstallationRequestsController', ['FactotumService', '$scope', 'objects', 'Product', function (FactotumService, $scope, objects, Product) {
    $scope.data = {objects: objects.data};
    $scope.data.objects = objects.data.map(function (elem) {
        var product = Product.get({id: elem.product_id});
        product.$promise.then(function (resp) {
            elem.product = resp
        });
        return elem;
    });
}]);


app.controller('UpdateInstallationController', ['$scope', '$rootScope', 'installation', 'stations', 'installation_types', '$state', function ($scope, $rootScope, installation, stations, installation_types, $state) {
    $scope.formData = installation;

    $scope.new = false;

    $scope.data.stations = stations.results;
    $scope.data.installation_types = installation_types.results;
    $scope.save = function () {
        if (!installation.station_id) {
            $rootScope.data.error.station_id = ['This field is required']
        }
        else if (!installation.installation_type_id) {
            $rootScope.data.error.installation_type_id = ['This field is required']
        }

        else {
            var station = $scope.data.stations.find(function (elem) {
                return elem.id === installation.station_id;
            });
            $scope.formData.location_id = station.location_id;
            $scope.formData.$update({id: installation.id}, function (data, status) {
                $state.go('installation_list')
            }, function (err) {
                $rootScope.data.error = err.data.message
            });
        }
    }
}]);

app.controller('AddInstallationController', ['$scope', '$rootScope', '$state', 'stations', 'installation_types', 'Installation', function ($scope, $rootScope, $state, stations, installation_types, Installation) {

    $scope.new = true;

    $scope.formData = {service_id: $rootScope.service.id, client_id: $rootScope.client.id};
    $scope.data = {stations: stations.results, installation_types: installation_types.results};
    $scope.data.products = $rootScope.service.products.filter(function (elem) {
        if (elem.is_network == false && elem.is_alert == false) {
            return elem
        }
    });

    $scope.save = function () {
        if (!$scope.formData.station_id) {
            $rootScope.data.error.station_id = ['This field is required']
        }
        else if (!$scope.formData.installation_type_id) {
            $rootScope.data.error.installation_type_id = ['This field is required']
        }

        else {
            var station = $scope.data.stations.find(function (elem) {
                return elem.id === $scope.formData.station_id;
            });
            $scope.formData.location_id = station.location_id;
            var inst = new Installation($scope.formData);
            inst.$save(function (data, status) {
                $state.go('installation_list')
            }, function (err) {
                console.log(err);
                $rootScope.data.error = err.data.message
            });
        }
    }
}]);

// ========================
// end of installation controllers
// ========================