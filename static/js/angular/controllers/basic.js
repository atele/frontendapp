// /**
//  * Created by stikks-workstation on 2/25/17.
//  */
//
'use strict';

var app = angular.module('atele.controllers.basic', ["pubnub.angular.service", 'atele.services']);

// ========================
// base controller
// ========================

app.controller('BaseController', function ($scope, $timeout, $q, $rootScope, $sce, FactotumService, PopupService, Notification) {

    $scope.init = function (service, client, navigations) {

        $rootScope.navigations = navigations.map(function (elem) {
            if (elem) {
                return JSON.parse(elem);
            }
        });

        $rootScope.current_nav = {'sub_navs': []};

        //$scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
        //    var nav = $rootScope.navigations.find(function (elem) {
        //        var _name = toState.url.split('/');
        //        return elem.code == _name[1]
        //    });
        //
        //    if (nav) {
        //        $scope.current_nav.sub_navs = nav.sub_navs;
        //    } else {
        //        $scope.current_nav.sub_navs = []
        //    }
        //});
        //$rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        //    console.log('resolved');
        //    var nav = $rootScope.navigations.find(function (elem) {
        //        var _name = current.$$route.originalPath.split('/');
        //        return elem.code == _name[1]
        //    });
        //
        //    if (nav) {
        //        $scope.current_nav.sub_navs = nav.sub_navs;
        //    } else {
        //        $scope.current_nav.sub_navs = []
        //    }
        //});

        jQuery(document).ready(function () {
            var pushWorker;
            var applicationServerKey = $.cookie('__publicKey__').replace(/\s/g, '+');
            var token = $.cookie('__cred__');

            function urlB64ToUint8Array(base64String) {
                const padding = '='.repeat((4 - base64String.length % 4) % 4);
                const base64 = (base64String + padding)
                    .replace(/\-/g, '+')
                    .replace(/_/g, '/');

                const rawData = window.atob(base64);
                const outputArray = new Uint8Array(rawData.length);

                for (var i = 0; i < rawData.length; ++i) {
                    outputArray[i] = rawData.charCodeAt(i);
                }
                return outputArray;
            }

            function updateServerSubscription(subscription) {
                $.ajax({
                    url: '/v1/push',
                    data: {'endpoint': JSON.stringify(subscription)},
                    beforeSend: function (request) {
                        request.setRequestHeader("Authorization", "Bearer " + token);
                    },
                    type: 'POST',
                    success: function (resp, status, xhr) {
                        console.log(resp);
                    },
                    error: function (xhr, status, err) {
                        console.log(err);
                    }
                })
            }

            function subscribeUser() {
                pushWorker.pushManager.subscribe({
                    userVisibleOnly: true,
                    applicationServerKey: urlB64ToUint8Array(applicationServerKey)
                }).then(function (subscription) {
                    updateServerSubscription(subscription);
                })
            }

            $rootScope.service = service;
            $rootScope.client = client;
            $rootScope.data = {'error': []};
            $rootScope.deleteObject = FactotumService.deleteObject;

            $scope.changeStates = function () {
                var country = $rootScope.countries.find(function (elem) {
                    return elem.id == $rootScope.form.country_id
                });
                $rootScope.states = country.states
            };

            $scope.changeCities = function () {
                var state = country.states.find(function (elem) {
                    return elem.id == $rootScope.form.state_id
                });
                $rootScope.cities = state.cities
            };

            $rootScope.trustedHtml = function (plainText) {
                return $sce.trustAsHtml(plainText);
            };

            $rootScope.deleteObject = FactotumService.deleteObject;
            $rootScope.popup = PopupService;

            Notification.query({is_read: false}).$promise.then(function (notes) {
                $rootScope.notifications = notes.data
            });

            if ('serviceWorker' in navigator && 'PushManager' in window) {
                navigator.serviceWorker.register('/sw.js').then(function (registration) {
                    // Registration was successful
                    console.log('ServiceWorker registration successful with scope: ', registration.scope);

                    pushWorker = registration;
                    pushWorker.pushManager.getSubscription().then(function (subscription) {
                        if (subscription === null) {
                            console.log('Subscribing user...');
                            subscribeUser();
                        }
                        else {
                            console.log('already subscribed')
                        }
                    })
                }, function (err) {
                    // registration failed
                    console.log('ServiceWorker registration failed: ', err);
                });
            }

            navigator.serviceWorker.onmessage = function (event) {
                var data = event.data;
                var title = 'Notification Alert';
                if (data.type == 'notification') {
                    if (data.title) {
                        title = data.title
                    }
                    var options = {
                        body: data.body,
                        icon: '/images/bell.png',
                        badge: '/images/badge.png'
                    };
                    pushWorker.showNotification(title, options);
                    $rootScope.notifications.push(data);
                }
                else {
                    $rootScope.popup.showDialog(title, data.body);
                }
            };
        });
    };
});

// ========================
// end of base controllers
// ========================

// ========================
// dashboard controllers
// ========================

app.controller('HomeController', function ($scope, $rootScope) {});

//app.controller('DashboardController', function ($scope, $rootScope, devices, installations, stations, networks) {
//   $scope.data = {installations: installations, devices: devices, stations: stations, networks: networks}
//});

app.controller('DashboardController', function ($scope) {
});

app.controller('DialogController', function ($scope, PopupService, title, body) {
    $scope.popup = PopupService;
    $scope.title = title;
    $scope.body = body;
});

// ========================
// end of dashboard controllers
// ========================