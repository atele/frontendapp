'use strict';

jQuery(document).ready(function () {

    var scope = angular.element($('#networksPage')).scope();

    if (scope.data != undefined) {

        var map = L.map('meshMap', {
            center: [9.0820, 8.6753],
            minZoom: 2,
            zoom: 6
        });
        L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
            subdomains: ['a', 'b', 'c']
        }).addTo(map);

        var myIcon = L.icon({
            iconUrl: '/static/images/blue-pin.png',
            iconRetinaUrl: '/static/images/blue-pin-md.png',
            iconSize: [29, 24],
            iconAnchor: [9, 21],
            popupAnchor: [0, -14]
        });

        setTimeout(function () {
            $.each(scope.data.objects, function (pos, device) {
                var dev_link = "#!/networks/" + device.id;
                L.marker([device.station.location.latitude, device.station.location.longitude], {icon: myIcon})
                    .bindPopup('<a href="' + dev_link + '" target="_self">#' + device.code + '</a>')
                    .addTo(map);
            });
        }, 1);
    }

});