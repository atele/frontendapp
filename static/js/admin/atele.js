/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    };
    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }
    };

}();

/***
Usage
***/
//Custom.init();
//Custom.doSomeStuff();

var SplashScreen = function () {

    return {
        //main function to initiate the module

        initSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").removeClass('visible');

        	$("#accept-button").click(function(e){
        		SplashScreen.hideSplashScreen();
        	});
        },

        showSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").addClass('visible');
        },

        hideSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").removeClass('visible');
        },

        init: function () {
        	// first initialize the splash screen
        	SplashScreen.initSplashScreen();

        	var show_splash = $.cookie('show_splash');
        	if(!show_splash) {
        		// show the spashscreen here then wait 2 hours and show it again
        		var date = new Date();
        		var minutes = 1;
        		date.setTime(date.getTime()) + (minutes * 60 * 1000);
        		$.cookie("show_splash", "true", {expires: 1, path: '/'});

        		SplashScreen.showSplashScreen();
        	}
        }
    };

}();

var DefaultSplashScreen = function () {

    return {
        //main function to initiate the module

        initDefaultSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").removeClass('visible');

        	$("#accept-button").click(function(e){
        		DefaultSplashScreen.hideDefaultSplashScreen();
        	});
        	$("#cancel-button").click(function(e){
        		DefaultSplashScreen.hideDefaultSplashScreen();
        	});
        },

        showDefaultSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").addClass('visible');
        },

        hideDefaultSplashScreen: function() {
        	$("#splash-screen, #splash-screen-dialog").removeClass('visible');
        },

        init: function () {
        	// first initialize the splash screen
        	DefaultSplashScreen.initDefaultSplashScreen();

        	var show_splash = $.cookie('show_splash');
        	DefaultSplashScreen.showDefaultSplashScreen();
        }
    };

}();