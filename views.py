from datetime import datetime
from flask import Blueprint, current_app as app
import htmlmin
from model_package.models import Service


def render_domain_template(name, **kwargs):
    from flask import render_template
    template_name = name

    _html = render_template(template_name, **kwargs)

    return htmlmin.minify(_html, remove_empty_space=True, remove_comments=True)


landing = Blueprint('landing', __name__)

with app.app_context():
    @landing.context_processor
    def main_context():
        """ Include some basic assets in the startup page """
        today = datetime.today()
        services = Service.query.all()
        return locals()


    @landing.route('/', methods=["GET"])
    @landing.route('/<path:url>/')
    def index(url=None):
        page_title = "Home"
        return render_domain_template('main/index.html', **locals())


    @landing.route('/services/', methods=["GET"])
    def atele_services():
        page_title = "Services"
        page_caption = "Directory"

        return render_domain_template('main/services.html', **locals())


    @landing.route('/devices/', methods=["GET"])
    def devices():
        page_title = "Hardware"
        page_caption = "Devices"

        return render_domain_template('main/devices.html', **locals())


    @landing.route('/pricing/', methods=["GET"])
    def pricing():
        page_title = "Pricing"
        page_caption = "Plan"

        return render_domain_template('main/pricing.html', **locals())


    @landing.route('/solutions/smart-metering/', methods=["GET"])
    def smart_metering():
        """
        view for smart grid services
        :return:
        """
        page_title = "Smart Grid Service"

        return render_domain_template('main/smart_grid.html', **locals())


    @landing.route('/solutions/mobile-asset/', methods=["GET"])
    def mobile_asset():
        """
        view for mobile assets
        :return:
        """
        page_title = "Mobile Asset Tracking Service"

        return render_domain_template('main/mobile_asset.html', **locals())


    @landing.route('/solutions/remote-monitoring/', methods=["GET"])
    def remote_monitoring():
        """
        view for remote monitoring services
        :return:
        """
        page_title = "Remote Monitoring Service"

        return render_domain_template('main/remote_monitoring.html', **locals())


# @landing.route('/sitemap.xml')
# def sitemap():
#     """
#     Generate sitemaps
#     :return:
#     """
#     return render_domain_template('main/sitemap.xml')