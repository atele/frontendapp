
from flask_script import Manager
from flask_assets import ManageAssets

from app import app

with app.app_context():

    manager = Manager(app)
    manager.add_command('assets', ManageAssets)

    # Allow the manager access config files on the fly
    manager.add_option('-c', '--config', dest='config_obj', default='settings.DevelopmentConfig', required=False)

    if __name__ == "__main__":
        manager.run()
