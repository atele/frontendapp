from flask import Flask
from flask_assets import Environment
from flask_cache import Cache

import settings

from model_package.models import db

app = Flask('main_app', template_folder='templates', static_folder='static')
app.config.from_object(settings.ProductionConfig)

db.init_app(app)
app.db = db

# initializing assets - js/css/sass
app.assets = Environment(app)
app.assets.from_yaml('asset.yml')
app.cache = Cache(app, config={'CACHE_TYPE': 'simple'})

with app.app_context():
    from views import landing

    app.register_blueprint(landing)

    if __name__ == '__main__':
        app.run(use_reloader=False, debug=False)
