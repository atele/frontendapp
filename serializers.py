import requests

from app import app

with app.app_context():

    class DataResource(object):
        BASE_URL = '{}/v1'.format(app.config['APP_URL'])


    class ServiceResource(DataResource):

        @classmethod
        def all(cls):
            """
            """
            return requests.get('{}/services'.format(cls.BASE_URL))