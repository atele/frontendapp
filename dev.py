from flask import Flask
from flask_sqlalchemy import SQLAlchemy

import settings

app = Flask('customer', template_folder='templates', static_folder='static')

with app.app_context():
    app.config.from_object(settings.DevelopmentConfig)
    db = SQLAlchemy()
    db.init_app(app)
    from views import landing
    app.register_blueprint(landing)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5960, debug=True)
